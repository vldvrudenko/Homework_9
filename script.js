"use strict";

let title = document.querySelectorAll(".tabs-title");
let tab= document.querySelectorAll(".tab");
let target;

title.forEach(function (tabclick) {
    tabclick.addEventListener("click", (e) => {
        title.forEach((tabclick) => {
            tabclick.classList.remove("active");
        });
        tabclick.classList.add("active");
        target = tabclick.getAttribute("data-target");
        changecontent(target);
    });
});

function changecontent(target) {
    tab.forEach((e) => {
        if (e.classList.contains(target)) {
            e.classList.add("active");
        } else e.classList.remove("active");
    });
}
